(ns testing-api.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [ring.middleware.json :as ring-json]
            [ring.util.request :as ring-req]
            [clj-http.client :as http]))

(def completeURLstring "http://census.daybreakgames.com/get/ps2:v2/character/?character_id=5428259567443978417&c:resolve=outfit_member_extended&c:resolve=stat_history&c:resolve=online_status&c:resolve=world&c:join=world")


(:name
 (:body
  (http/get completeURLstring {:query-params {} ;; you can submit query params here
                               :as :json})))

(defn call-player-stat []
  (:body
   (http/get completeURLstring {:query-params {} ;; you can submit query params here
                                :as :json})))

(call-player-stat)




(defroutes app-routes
  (GET "/" [] "Hello World")
  (GET "/alexor/player" [] "testapi")
  (route/not-found "Not Found"))

(def app
  (wrap-defaults app-routes site-defaults))
